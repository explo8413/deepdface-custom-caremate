from flask import Flask, request, jsonify
import os
import base64
import deepface
from deepface import DeepFace


app = Flask(__name__)

@app.route('/ping', methods=['GET'])
def ping():
    return jsonify({"message": "Pong!"}), 200

@app.route('/check', methods=['POST'])
def upload_image():
    try:
        # Get the image data from the request
        img_data = request.form.get('img')

        # Decode base64 image data
        img_data_decoded = base64.b64decode(img_data)

        # Specify the folder to save the images
        upload_folder = 'toBeDetermined'

        # Create the folder if it doesn't exist
        if not os.path.exists(upload_folder):
            os.makedirs(upload_folder)

        # Save the image to the specified folder with the filename 'img1'
        img_path = os.path.join(upload_folder, 'img1.jpg')

        if os.path.exists(img_path):
            os.remove(img_path)

        with open(img_path, 'wb') as img_file:
            img_file.write(img_data_decoded)

        best = {
            "distance": 1,
            "nom": 0
        }
        i = len(os.listdir(upload_folder))
        print(i)
        for x in range(1, i-1):
            imgName = 'toBeDetermined/img' + str(x+1) + '.jpg'
            result = DeepFace.verify(img1_path="toBeDetermined/img1.jpg", img2_path=imgName)
            print(result)
            if result["distance"] < best["distance"]:
                best["distance"] = result["distance"]
                if x == 2:
                    best["nom"] = "Dupont"
                if x == 3:
                    best["nom"] = "Marc"
                if x == 2:
                    best["nom"] = "Durand"

        #result = DeepFace.verify(img1_path="toBeDetermined/img2.jpg", img2_path="img2.jpg")


        # Return a success response
        return jsonify({"message": best["nom"]}), 200

    except Exception as e:
        # Return an error response if something goes wrong
        return jsonify({"error": str(e)}), 500

if __name__ == '__main__':
    app.run(debug=True)
